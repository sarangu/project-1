var app = angular.module('admin', ['ui.router', 'ui.bootstrap', 'toastr']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login', {
            name: 'login',
		    url: '/login',
		    templateUrl: './app/modules/auth/auth-login.view.html',
	    	controller: 'AuthCtrl',
	    	controllerAs: 'authCtrl'
        })
        .state('dashboard', {
            name: 'dashboard',
		    url: '/dashboard',
		    templateUrl: './app/modules/dashboard/dashboard.view.html',
	    	controller: 'DashboardCtrl',
	    	controllerAs: 'dashboardCtrl'
        })
        .state('dashboard.employees', {
            name: 'employees',
		    url: '/employees',
		    templateUrl: './app/modules/employees/employees-list.view.html',
	    	controller: 'EmpCtrl',
	    	controllerAs: 'empCtrl'
        });

});
