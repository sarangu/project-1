var app = angular.module('admin');

app.controller('AuthCtrl', ['$scope', '$state', function ($scope, $state) {
	var vm = this;

	vm.signin = function(data) {
		console.log(data);
		$state.go('dashboard');
	}
}]);