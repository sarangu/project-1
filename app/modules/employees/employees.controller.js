var app = angular.module('admin');

app.controller('EmpCtrl', ['$scope', '$rootScope', '$state', '$uibModal', function ($scope, $rootScope, $state, $uibModal) {
	var vm = this;

	$rootScope.empArray = [{
		id: 1,
		name: 'sankar',
		number: 8885188893,
		email: 'sankar@test.com'
	}];

	 vm.openModal = function () {
	    var modalInstance = $uibModal.open({
	      templateUrl: './app/modules/employees/emp-add.view.html',
	      controller: 'ModalInstanceCtrl',
	      controllerAs: 'modalCtrl',
	      size: 'md',
	      resolve: {
	        empService: function () {
	          return {};
	        }
	      }
	    });
	}

	 vm.openView = function (data) {
	    var modalInstance = $uibModal.open({
	      templateUrl: './app/modules/employees/emp-edit.view.html',
	      controller: 'ModalInstanceCtrl',
	      controllerAs: 'modalCtrl',
	      size: 'md',
	      resolve: {
	        empService: function () {
	          return angular.copy(data);
	        }
	      }
	    });
	}

	 vm.deleteView = function (data) {
	    var modalInstance = $uibModal.open({
	      templateUrl: './app/modules/employees/emp-delete.view.html',
	      controller: 'ModalInstanceCtrl',
	      controllerAs: 'modalCtrl',
	      size: 'sm',
	      resolve: {
	        empService: function () {
	          return angular.copy(data);
	        }
	      }
	    });
	}

}]);

app.controller('ModalInstanceCtrl', ['$scope', '$rootScope', '$uibModalInstance', 'empService', function ($scope, $rootScope, $uibModalInstance, empService) {
	var modalCtrl = this;

	modalCtrl.editObj = empService;

	modalCtrl.close = function() {
		$uibModalInstance.close();
	}

	modalCtrl.newEmployee = function(data) {
		data.id = $rootScope.empArray.length + 1;
		$rootScope.empArray.push(data);
		// console.log(data);
		modalCtrl.close();
	}

	modalCtrl.updateEmployee = function(data) {
		// console.log(data);
		angular.forEach($rootScope.empArray, function(v, k) {
			if (v.id == data.id) {
				v.name = data.name;
				v.email = data.email;
				v.number = data.number;
			}
		});
		modalCtrl.close();
	}

	modalCtrl.delete = function() {
		$rootScope.empArray.splice(modalCtrl.editObj, 1);
		modalCtrl.close();
	}

}])